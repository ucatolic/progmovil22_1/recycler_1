package com.sigmotoa.characters
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import androidx.recyclerview.widget.RecyclerView

class CharacterAdapter (val characterList:Array<String>):
    RecyclerView.Adapter<CharacterAdapter.CharacterViewHolder>()
{

    //Describe el item para el recyclerview
    class CharacterViewHolder(itemView: View):RecyclerView.ViewHolder(itemView) {
        private val characterTextView:TextView=itemView.findViewById(R.id.character_text)

        fun bind(word: String){
            characterTextView.text=word
        }

    }

        // Retorna un nuevo ViewHolder
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CharacterAdapter.CharacterViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.character_item, parent, false)
        return CharacterViewHolder(view)
    }

    // Muestras los datos/items en las respectivas posiciones
    override fun onBindViewHolder(holder: CharacterAdapter.CharacterViewHolder, position: Int) {
        holder.bind(characterList[position])
    }

    // Retorna el tamaño de la lista
    override fun getItemCount(): Int {
        return characterList.size
    }
}










